<?php
$_GET['file'] = basename(__FILE__);
require(__DIR__ . '/log.php');

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

use HelloWorld\SayHello;

echo SayHello::world();