FROM php:apache
WORKDIR /var/www
COPY src/ /var/www/html/
RUN apt-get upgrade && docker-php-ext-install mysqli pdo pdo_mysql
EXPOSE 80
CMD ["apache2-foreground"]
